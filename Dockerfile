FROM miniflux/miniflux:2.0.28

USER root

# No extension, or else script won't run
COPY reset-feed-errors /etc/periodic/daily/
RUN chmod +x /etc/periodic/daily/reset-feed-errors

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

CMD []
ENTRYPOINT ["/docker-entrypoint.sh"]
